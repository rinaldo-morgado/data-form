
<header>
	<div class="navbar">
		<div class="container-fluid">
    		<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-warning-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="../home" class="navbar-brand">QualityPress - Formul�rios</a>
            </div>
            <div class="navbar-collapse collapse navbar-warning-collapse">
                <ul class="nav navbar-nav">
                    
                    <li class="dropdown">
                      <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Gateways
                        <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                                <li class="dropdown-header">Pagamentos</li>
                                <li><a href="../gateway/superpay.php">SuperPay</a></li>
                                <li><a href="../gateway/pagseguro.php">PagSeguro</a></li>
                                <li><a href="../gateway/paypal.php">PayPal</a></li>
                                
                                <li class="divider"></li>
                                <li class="dropdown-header">Marketplaces</li>
                                <li><a href="../gateway/pluggto.php">Plugg.to</a></li>
                                <li><a href="../gateway/mercadolivre.php">Mercado Livre</a></li>
                        </ul>
                    </li>                    
                    
                    <li class="dropdown">
                      <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Formas de Pagamento
                        <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li class="dropdown-header">Operadoras de Cr�dito</li>
                        <li><a href="../pagamentos/cielo.php">Cielo</a></li>
                        <li><a href="../pagamentos/redecard.php">RedeCard</a></li>
                        
                        <li class="divider"></li>
                        <li class="dropdown-header">Boleto Banc�rio</li>
                        <li><a href="../pagamentos/shopline.php">Ita� Shopline</a></li>
                        <li><a href="../pagamentos/shopfacil.php">Bradesco Shopf�cil</a></li>

                      </ul>
                    </li>
                    
                    <li><a href="../host">Hospedagem</a></li>
                    <li><a href="../erp">Contato ERP</a></li>
                    <li><a href="../clear">ClearSale</a></li>
                
                </ul>
            </div>
        </div>
    </div>
</header>