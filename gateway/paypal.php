<?php include '../config.php';?>


<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Ordem de Servi�o:',$_POST["order"],' ',
	'E-mail: ',$_POST["email"],' ',
	'Senha: ',$_POST["password"],' ',
  ' ',
	'Dados enviados �s ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);

$newFile = __DIR__.'/../received/'.$_POST["order"]."_paypal_".".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);

sendMail($_POST["order"] . ' - Dados PayPal', $fields, 'PayPal');

echo '<div class="alert alert-dismissible alert-success" style="text-align:center;" ><a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Informa��es enviadas com sucesso!</strong> </div>';


}

?>


<!DOCTYPE html>
<html>

<?php include '../head.php';?>

<body>

<?php include '../menu.php';?>
<main>

    <div class="container">


            <div class="row">
              <div class="col-xs-12 col-md-8 col-md-offset-2">

        <div class="jumbotron">
            
            <div class="row"> 
              <div class="col-md-8">
            <h2>PayPal</h2>
                
              </div>
              <div class="col-md-4">
            <img src="../assets/img/logo.png" class="img-responsive">
                  
              </div>
            </div>     
            
            <h5>Para realizar transa��es via PayPal, acesse este <a href="https://www.paypal.com/br/home" target="_blank" data-toggle="tooltip" title="Somente se n�o tiver conta.">link</a> e crie sua conta de vendedor. </h5> <h5>Caso tenha d�vidas, entre em contato com nossa equipe de <a href="#" data-toggle="modal" data-target="#contato">projetos</a>.</h5>

            <!-- Modal -->
            <?php include '../modal.php';?>


            <form method="POST" >
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault"><?php echo $staticLabels['numero_os'];?></label>
                  <input type="number"  class="form-control order" name="order" tabindex="1" required autofocus>
                  <p class="help-block">N�mero da ordem de servi�o do projeto do ecommerce.</p>
                </div>
                
               
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">E-mail de login</label>
                  <input type="email" class="form-control" name="email" tabindex="2" required>
                  <p class="help-block">E-mail utilizado no cadastro do PayPal.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault" >Senha</label>
                   <div class="input-group">
                      <input type="password" class="form-control" name="password" tabindex="3" required>
                          <span class="input-group-btn">
                                <button type="button"  class="eye btn-pwd btn btn-fab btn-fab-mini " tabindex="8">
                                  <i class="fa fa-eye" aria-hidden="true"></i></button>    
                      </div> <p class="help-block">Senha de acesso a conta do PayPal</p>
                </div>


        <div class="form-group">
        <h4>Esta conta est� sendo utilizada?</h4>
          <div class="radio">
            <label>
              <input name="uso" value="Conta em utiliza��o" checked="" type="radio" tabindex="4">
              Sim. Esta conta est� em uso no momento em outra plataforma de e-commerce.
            </label>
          </div>
          <div class="radio">
            <label>
              <input name="uso" value="Conta n�o est� em uso" type="radio" tabindex="5">
              N�o. Esta conta <b>n�o</b> est� um uso em outra plataforma de e-commerce.
            </label>
          </div>
        </div>


                <div class="form-group">
                   <div style="text-align: right; padding-top: 10px;" >
                     <button type="submit" class="btn btn-primary btn-raised send" tabindex="6" ><?php echo $staticLabels['send_btn'];?></button>
                     <button type="reset" class="btn btn-default" tabindex="7"><?php echo $staticLabels['cancel_btn'];?></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

        </div>
    </div>
    
</main>

</body>
<?php include '../footer.php';?>
</html>
