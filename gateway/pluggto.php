<?php include '../config.php';?>


<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Ordem de Servi�o:',$_POST["order"],' ',
	'E-mail: ',$_POST["email"],' ',
	'Senha: ',$_POST["password"],' ',
  ' ',
	'Dados enviados �s ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);

/*echo $fields;
die;
*/

$newFile = __DIR__.'/../received/'.$_POST["order"]."_pluggto_".".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);

sendMail($_POST["order"] . ' - Dados Plugg.to', $fields, 'Plugg.to');

echo '<div class="alert alert-dismissible alert-success" style="text-align:center;" ><a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Informa��es enviadas com sucesso!</strong> </div>';


}

?>


<!DOCTYPE html>
<html>

<?php include '../head.php';?>

<body>

<?php include '../menu.php';?>

<main>

    <div class="container">


            <div class="row">
              <div class="col-xs-12 col-md-8 col-md-offset-2">

        <div class="jumbotron">
            
            <div class="row"> 
              <div class="col-md-8">
            <h2>Plugg To</h2>
                
              </div>
              <div class="col-md-4">
            <img src="../assets/img/logo.png" class="img-responsive">
                  
              </div>
            </div>     
            


            <form method="POST" >
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault"><?php echo $staticLabels['numero_os'];?></label>
                  <input type="number"  class="form-control order" name="order" tabindex="1" required autofocus>
                  <p class="help-block">N�mero da ordem de servi�o do projeto do ecommerce.</p>
                </div>
                
               
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">E-mail de login</label>
                  <input type="email" class="form-control" name="email" tabindex="2" required>
                  <p class="help-block">E-mail utilizado no cadastro do Plugg.To.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault" >Senha</label>
                   <div class="input-group">
                      <input type="password" class="form-control"  name="password" tabindex="3" required>
                          <span class="input-group-btn">
                                <button type="button"  class="eye btn-pwd btn btn-fab btn-fab-mini " tabindex="6">
                                  <i class="fa fa-eye" aria-hidden="true"></i></button>    
                      </div> <p class="help-block">Senha de acesso a conta do Plugg.To</p>
                </div>


                <div class="form-group">
                   <div style="text-align: right; padding-top: 10px;" >
                     <button type="submit" class="btn btn-primary btn-raised send" tabindex="4" ><?php echo $staticLabels['send_btn'];?></button>
                     <button type="reset" class="btn btn-default" tabindex="5"><?php echo $staticLabels['cancel_btn'];?></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

        </div>
    </div>
    
</main>

</body>
<?php include '../footer.php';?>
</html>
