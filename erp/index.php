<?php include '../config.php';?>


<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Ordem de Servi�o:',$_POST["order"],' ',
	'ERP a ser intgrado: ',$_POST["erp"],' ',
	'Site do ERP: ',$_POST["erpweb"],' ',
	'Contato ERP: ',$_POST["erpcontact"],' ',
	'E-mail contato: ',$_POST["emailerp"],' ',
  'Telefone de contato: ',$_POST["telerp"],' ',
  'Observa��es: ',$_POST["noteerp"],' ',
	' ',
	'Dados enviados �s ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);

$newFile = __DIR__.'/../received/'.$_POST["order"]."_ERP_".".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);

sendMail($_POST["order"] . ' - Dados ERP', $fields, 'ERP');

echo '<div class="alert alert-dismissible alert-success" style="text-align:center;" ><a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Informa��es enviadas com sucesso!</strong> </div>';


}

?>


<!DOCTYPE html>
<html>
<?php include '../head.php';?>
<body>

<?php include '../menu.php';?>
<main>
    <div class="container">


            <div class="row">
              <div class="col-xs-12 col-md-8 col-md-offset-2">

        <div class="jumbotron">
            
            <div class="row"> 
              <div class="col-md-8">
            <h2>Integra��o ERP</h2>
                 
              </div>
              <div class="col-md-4">
            <img src="../assets/img/logo.png" class="img-responsive">
                  
              </div>
            </div>     
            


            <form method="POST" >
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault"><?php echo $staticLabels['numero_os'];?></label>
                  <input type="number" class="form-control order" name="order" required autofocus>
                  <p class="help-block">N�mero da ordem de servi�o do projeto do ecommerce.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" >ERP a ser integrado</label>
                  <input type="text" class="form-control" name="erp" required>
                  <p class="help-block">Nome do ERP a ser integrado com o Q.COMMERCE.</p>
                </div>
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">Site ERP</label>
                  <input type="url" class="form-control" name="erpweb" required>
                  <p class="help-block">Site do sistema de ERP.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">Nome do contato do ERP</label>
                  <input type="text" class="form-control"  name="erpcontact" required>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">E-mail de contato do representante comercial do ERP</label>
                  <input type="email" class="form-control" name="emailerp" required>
                  <p class="help-block">E-mail do comercial respons�vel por sua empresa no ERP.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">Telefone de contato ERP</label>
                  <input type="text" class="form-control phone_with_ddd" name="telerp" required>
                  <p class="help-block">Telefone do comercial respons�vel por sua empresa no ERP. Ex.: (XX) 9 9999-9999</p>
                </div>

                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">Observa��o</label>
                  <textarea rows="4" class="form-control" name="noteerp"></textarea>
                  <p class="help-block">Observa��es sobre a integra��o</p>
                </div>
                

                  <div class="form-group">
                    <div style="text-align: right; padding-top: 10px;">
                      <button type="submit" name="teu-btn-submit" class="send btn btn-primary btn-raised"><?php echo $staticLabels['send_btn'];?></button>
                      <button type="reset" class="btn btn-default"><?php echo $staticLabels['cancel_btn'];?></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

        </div>
    </div>
</main>

</body>
<?php include '../footer.php';?>
</html>
