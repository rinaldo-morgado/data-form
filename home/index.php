<?php include '../config.php';?>


<!DOCTYPE HTML>

<html>

<?php include '../head.php';?>

<body>

<?php include '../menu.php';?>

	<main>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-10 col-md-offset-1">
					<div class="jumbotron">
						<div class="row titulo"> 
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<h1>Bem vindo!</h1>
							</div>
							<div class="col-lg-4 col-xs-offset-1 col-md-4 col-sm-4 col-xs-4">
							<img src="../assets/img/logo.png" class="img-responsive">
							</div>
						</div>

						<div class="row-content titulo">Voc&ecirc; est&aacute; acessando a plataforma de formul&aacute;rios da QualityPress. Atrav&eacute;s dos &iacute;cones abaixo e do menu no topo, selecione qual formul&aacute;rio deseja preencher!</div>



						<div class="row marginrow">
							<a href="../pagamentos/cielo.php">
								<div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-3 col-xs-offset-1 show">
									<img class="circle img-responsive imgresponsive" src="../assets/img/cielo.png" alt="cielo">

								</div>						
								<div class="col-md-8 col-sm-7 col-xs-6">
									<h3 class="nostyle">Formul&aacute;rio de coleta de dados da Cielo</h3>
								</div>
							</a>
								
						</div>

						<div class="row marginrow">
							<a href="../host">
								<div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-3 col-xs-offset-1">
									<img class="circle img-responsive imgresponsive" src="../assets/img/host.png" alt="hospedagem">
								</div>						
								<div class="col-md-8 col-sm-7 col-xs-6">
									<h3 class="nostyle">Formul&aacute;rio de coleta de informa&ccedil;&otilde;es da Hospedagem</h3>
								</div>
							</a>
						</div>

						<div class="row marginrow">

							<a href="../erp">
								<div class="col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-3 col-xs-offset-1">
									<img class="circle img-responsive imgresponsive" src="../assets/img/erp.png" alt="erp">
								</div>						
								<div class="col-md-8 col-sm-7 col-xs-6">
									<h3 class="nostyle">Formul&aacute;rio de coleta de contato do ERP</h3>
								</div>
							</a>
					
						</div>

						<hr>
							
							<div class="col-md-8 col-md-offset-2 doubt">
								<a href="http://qualitypress.com.br/contato#form-contato">D&uacute;vidas? Fale conosco!</a>
								<p></p>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</body>
</html>

<?php include '../footer.php';?>