<?php include '../config.php';?>



<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Ordem de Servi�o:',$_POST["order"],' ',
	'Chave de criptografia: ',$_POST["key"],' ',
	'C�digo do site: ',$_POST["code"],' ',
  ' ',
	'Dados enviados �s ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);

/*echo $fields;
die;
*/

$newFile = __DIR__.'/../received/'.$_POST["order"]."_shopline_".".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);

sendMail($_POST["order"] . ' - Dados Ita� Shopline', $fields, 'Ita� Shopline');

echo '<div class="alert alert-dismissible alert-success" style="text-align:center;" ><a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Informa��es enviadas com sucesso!</strong> </div>';


}

?>


<!DOCTYPE html>
<html>
<?php include '../head.php';?>

<body>

<?php include '../menu.php';?>

<main>

    <div class="container">


            <div class="row">
              <div class="col-xs-12 col-md-8 col-md-offset-2">

        <div class="jumbotron">
            
            <div class="row"> 
              <div class="col-md-8">
            <h2>Ita� Shopline</h2>
                 
              </div>
              <div class="col-md-4">
            <img src="../assets/img/logo.png" class="img-responsive">
                  
              </div>
            </div>     
            
            <h5>Para utilizar o Ita� Shopline, acesse o seu bankline e em recebimentos, localize a op��o shopline e preencha o fomul�rio de contrata��o.</h5><h5>Caso tenha d�vidas, entre em contato com nossa equipe de <a href="#" data-toggle="modal" data-target="#contato">projetos</a> ou com o gerente de sua conta.</h5>

            <!-- Modal -->
            <?php include '../modal.php';?>

            <form method="POST" >
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault"><?php echo $staticLabels['numero_os'];?></label>
                  <input type="number" class="form-control order" name="order" tabindex="1" required autofocus>
                  <p class="help-block">N�mero da ordem de servi�o do projeto do ecommerce.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" >Chave de criptografia</label>
                  <input type="text" class="form-control" maxlength="16" name="key" tabindex="2" required>
                  <p class="help-block">C�digo de 16 posi��es. Pode ser obtido no m�dulo Ita� Shopline acessado via Ita� Bankline.</p>
                </div>

                <div class="form-group label-floating is-empty">
                  <label class="control-label" >C�digo do site</label>
                  <input type="text" class="form-control" maxlength="26" name="code" tabindex="3" required>
                  <p class="help-block">C�digo de 26 posi��es. Pode ser obtido no m�dulo Ita� Shopline acessado via Ita� Bankline.</p>
                </div>


                  <div class="form-group">
                    <div style="text-align: right; padding-top: 10px;">
                      <button type="submit" class="send btn btn-primary btn-raised" tabindex="4"><?php echo $staticLabels['send_btn'];?></button>
                      <button type="reset" class="btn btn-default" tabindex="5"><?php echo $staticLabels['cancel_btn'];?></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

        </div>
    </div>
    
</main>


</body>
<?php include '../footer.php';?>
</html>
