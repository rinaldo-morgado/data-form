<?php include '../config.php';?>



<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Ordem de Servi�o:',$_POST["order"],' ',
	'Chave de criptografia: ',$_POST["assinatura"],' ',
	'C�digo da loja: ',$_POST["code"],' ',
  'Ag�ncia: ',$_POST["agency"],' ',
  'Conta: ',$_POST["account"],' ',
  'Usu�rio do painel Bradesco ShopFacil (MUP): ',$_POST["user"],' ',
  'Senha do painel Bradesco ShopFacil (MUP): ',$_POST["password"],' ',
  ' ',
	'Dados enviados �s ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);

/*echo $fields;
die;
*/
$newFile = __DIR__.'/../received/'.$_POST["order"]."_shopfacil_".".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);

sendMail($_POST["order"] . ' - Dados Bradesco Shopf�cil', $fields, 'Bradesco Shopf�cil');

echo '<div class="alert alert-dismissible alert-success" style="text-align:center;" ><a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Informa��es enviadas com sucesso!</strong> </div>';


}

?>


<!DOCTYPE html>
<html>
<?php include '../head.php';?>

<body>

<?php include '../menu.php';?>

<main>

    <div class="container">


            <div class="row">
              <div class="col-xs-12 col-md-8 col-md-offset-2">

        <div class="jumbotron">
            
            <div class="row"> 
              <div class="col-md-8">
            <h2>Bradesco Shopf�cil</h2>
                 
              </div>
              <div class="col-md-4">
            <img src="../assets/img/logo.png" class="img-responsive">
                  
              </div>
            </div>     
            
            <h5>Para utilizar o Bradesco Shopf�cil, entre em contato com seu gerente de conta, e solicite a utiliza��o do Boleto Shopf�cil atrav�s do kit da Scopus.</h5><h5>Caso tenha d�vidas, entre em contato com nossa equipe de <a href="#" data-toggle="modal" data-target="#contato">projetos</a> ou com o gerente de sua conta.</h5>

            <!-- Modal -->
            <?php include '../modal.php';?>

            <form method="POST" >
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault"><?php echo $staticLabels['numero_os'];?></label>
                  <input type="number" class="form-control order" name="order" tabindex="1" required autofocus>
                  <p class="help-block">N�mero da ordem de servi�o do projeto do ecommerce.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" >Assinatura Digital</label>
                  <input type="text" class="form-control" name="assinatura" tabindex="2" required>
                  
                </div>

                <div class="form-group label-floating is-empty">
                  <label class="control-label" >C�digo da loja</label>
                  <input type="text" class="form-control" name="code" tabindex="3" required>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" >Ag�ncia</label>
                  <input type="number" class="form-control" name="agency" tabindex="4" required>
                </div>

                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" >Conta</label>
                  <input type="number" class="form-control" name="account" tabindex="5" required>
                </div>

                <div class="form-group label-floating is-empty">
                  <label class="control-label" >Usu�rio do painel Bradesco ShopFacil (MUP)</label>
                  <input type="text" class="form-control" name="user" tabindex="6" required>
                </div>

                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault" >Senha do painel Bradesco ShopFacil (MUP)</label>
                   <div class="input-group">
                      <input type="password" class="form-control" name="password" tabindex="7" required>
                          <span class="input-group-btn">
                            <button type="button" class="eye btn-pwd btn btn-fab btn-fab-mini " tabindex="10">
                              <i class="fa fa-eye" aria-hidden="true"></i>
                            </button>    
                    </div> 
                </div>



                  <div class="form-group">
                    <div style="text-align: right; padding-top: 10px;">
                      <button type="submit" class="send btn btn-primary btn-raised" tabindex="8"><?php echo $staticLabels['send_btn'];?></button>
                      <button type="reset" class="btn btn-default" tabindex="9"><?php echo $staticLabels['cancel_btn'];?></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

        </div>
    </div>
    
</main>


</body>
<?php include '../footer.php';?>
</html>
