<?php include '../config.php';?>


<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Ordem de Servi�o:',$_POST["order"],' ',
	'Estabelecimento: ',$_POST["store"],' ',
	'E-mail do respons�vel: ',$_POST["email"],' ',
	'CNPJ: ',$_POST["cnpj"],' ',
	'Nome Fantasia: ',$_POST["fancyname"],' ',
  ' ',
	'Dados enviadOs �s ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);

$newFile = __DIR__.'/../received/'.$_POST["order"]."_cielo_".".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);

sendMail($_POST["order"] . ' - Dados Cielo', $fields, 'Cielo');

echo '<div class="alert alert-dismissible alert-success" style="text-align:center;" ><a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Informa��es enviadas com sucesso!</strong> </div>';


}

?>


<!DOCTYPE html>
<html>

<?php include '../head.php';?>

<body>

<?php include '../menu.php';?>
<main>

    <div class="container">


            <div class="row">
              <div class="col-xs-12 col-md-8 col-md-offset-2">

        <div class="jumbotron">
            
            <div class="row"> 
              <div class="col-md-8">
            <h2>Cielo</h2>
                 
              </div>
              <div class="col-md-4">
            <img src="../assets/img/logo.png" class="img-responsive">
                  
              </div>
            </div>     
            
            <h5>Para realizar cadastro junto a Cielo, clique <a href="https://www.cielo.com.br/credenciamento" target="_blank">aqui</a>.</h5> <h6>Obs.: Realize o cadastro na modalidade <b>WebService</b></h6> <h5>Caso tenha d�vidas, entre em contato com nossa equipe de <a href="#" data-toggle="modal" data-target="#contato">projetos</a>.</h5>

            <!-- Modal -->
            <?php include '../modal.php';?>

            <form method="POST" >
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault"><?php echo $staticLabels['numero_os'];?></label>
                  <input type="number"  class="form-control order" name="order" required autofocus>
                  <p class="help-block">N�mero da ordem de servi�o do projeto do ecommerce.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" >Estabelecimento Cielo</label>
                  <input type="number" class="form-control" name="store" required>
                  <p class="help-block">N�mero de estabelecimento recebido ao finalizar contrato com a Cielo.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">E-mail do respons�vel pelo cadastro</label>
                  <input type="email" class="form-control" name="email" required>
                  <p class="help-block">E-mail utilizado no cadastro junto a Cielo.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">CNPJ da loja</label>
                  <input type="text" class="form-control cnpj" data-mask="00/00/0000" name="cnpj" required>
                  <p class="help-block">CNPJ utilizado no cadastro junto a Cielo.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">Nome Fantasia</label>
                  <input type="text" class="form-control"  name="fancyname" required>
                </div>


                <div class="form-group">
                  <h4>Este estabelecimento est� sendo utilizado?</h4>
                    <div class="radio">
                      <label>
                        <input name="uso" value="Conta em utiliza��o" checked="" type="radio" >
                        Sim. Este estabelecimento est� em uso no momento em outra plataforma de e-commerce.
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input name="uso" value="Conta n�o est� em uso" type="radio" >
                        N�o. Este estabelecimento <b>n�o</b> est� um uso em outra plataforma de e-commerce.
                      </label>
                    </div>
                  </div>





                  <div class="form-group">
                    <div style="text-align: right; padding-top: 10px;">
                      <button type="submit" class="btn btn-primary btn-raised send"><?php echo $staticLabels['send_btn'];?></button>
                      <button type="reset" class="btn btn-default"><?php echo $staticLabels['cancel_btn'];?></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

        </div>
    </div>
    
</main>

</body>
<?php include '../footer.php';?>
</html>
