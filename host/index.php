<?php include '../config.php';?>

<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Ordem de Servi�o:',$_POST["order"],' ',
	'Nome fantasia: ',$_POST["fancyname"],' ',
	'Dom�nio: ',$_POST["domain"],' ',
	'E-mails a serem criados: ',$_POST["emailslist"],' ',
	'Observa��es: ',$_POST["note"],' ',
	' ',
	'Dados enviados �s ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);

$newFile = __DIR__.'/../received/'.$_POST["order"]."_host_".".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);

sendMail($_POST["order"] . ' - Dados de Hospedagem', $fields, 'Hospedagem');

echo '<div class="alert alert-dismissible alert-success" style="text-align:center;" ><a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Informa��es enviadas!</strong> </div>';
}
?>

<!DOCTYPE HTML>
<html>
<?php include '../head.php';?>
<body>

<?php include '../menu.php';?>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="jumbotron">
          <div class="row"> 
            <div class="col-md-8">
              <h2>Hospedagem</h2>
            </div>
            <div class="col-md-4">
              <img src="../assets/img/logo.png" class="img-responsive">
            </div>
          </div>     

            <form method="POST" name="bannerform" >
              
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault"><?php echo $staticLabels['numero_os'];?></label>
                  <input type="number" class="form-control order" name="order" required autofocus>
                  <p class="help-block">N�mero da ordem de servi�o na QualityPress.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <input type="text" class="form-control" name="fancyname" required>
                  <label class="control-label" for="inputDefault">Nome fantasia </label> 
                  <p class="help-block">Nome fantasia do ecommerce</p>
                </div>
               
               <div class="form-group label-floating is-empty">
                  <input type="text" class="form-control" name="domain" required>
                  <label class="control-label" for="inputDefault">Dom�nio a ser utilizado</label> 
                  <p class="help-block">Dom�nio a ser utilizado para o desenvolvimento da plataforma.</p>
                </div>

                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">E-mails a serem criados</label>
                  <textarea rows="4" class="form-control"  name="emailslist"></textarea>
                  <p class="help-block">E-mails com seu dom�nio para serem criados/migrados, e.g.: financeiro@meudominio.com.br</p>
                </div>

                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault">Observa��es</label>
                  <textarea rows="4" class="form-control"  name="note"></textarea>
                  
                </div>
                
                <div class="form-group">
                    <div style="text-align: right; padding-top: 10px;">
                      <button type="submit" class="btn btn-primary btn-raised send"><?php echo $staticLabels['send_btn'];?></button>
                      <button type="reset" class="btn btn-default"><?php echo $staticLabels['cancel_btn'];?></button>
                    </div>
                </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</body>
<?php include '../footer.php';?>
</html>
