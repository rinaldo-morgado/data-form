<?php include '../config.php';?>

<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Ordem de Servi�o:',$_POST["order"],' ',
	'E-mail de acesso: ',$_POST["user"],' ',
	'Senha: ',$_POST["password-clear"],' ',
	'Token Produ��o: ',$_POST["token_prod"],' ',
	'Token Homologa��o: ',$_POST["token_dev"],' ',
	' ',
	'Dados enviados �s ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);

$newFile = __DIR__.'/../received/'.$_POST["order"]."_clearsale_".".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);

sendMail($_POST["order"] . ' - Dados ClearSale', $fields, 'ClearSale');

echo '<div class="alert alert-dismissible alert-success" style="text-align:center;" ><a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Informa��es enviadas!</strong> </div>';

}
?>

<!DOCTYPE HTML>
<html>
<?php include '../head.php';?>
<body>

<?php include '../menu.php';?>

  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="jumbotron">
          <div class="row"> 
            <div class="col-md-8">
              <h2>ClearSale</h2>
            </div>
            <div class="col-md-4">
              <img src="../assets/img/logo.png" class="img-responsive">
            </div>
          </div>     

          <h5>Caso tenha d�vidas, entre em contato com nossa equipe de <a href="#" data-toggle="modal" data-target="#contato">projetos</a>.</h5>
                      <?php include '../modal.php';?>
            <form method="POST" name="bannerform" >
              
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault"><?php echo $staticLabels['numero_os'];?></label>
                  <input type="number" class="form-control order" name="order" required autofocus>
                  <p class="help-block">N�mero da ordem de servi�o na QualityPress.</p>
                </div>
                
                <div class="form-group label-floating is-empty">
                  <input type="email" class="form-control" name="user" required autocomplete="false">
                  <label class="control-label" for="inputDefault">E-mail de acesso</label> 
                  <p class="help-block">E-mail de acesso a plataforma do ClearSale.</p>
                </div>
               
                <div class="form-group label-floating is-empty">
                  <label class="control-label" for="inputDefault" >Senha</label>
                   <div class="input-group">
                      <input type="password" class="form-control" name="password-clear" required autocomplete="false">
                          <span class="input-group-btn">
                                <button type="button" class="eye btn-pwd btn btn-fab btn-fab-mini">
                                  <i class="fa fa-eye" aria-hidden="true"></i></button>    
                      </div> <p class="help-block">Senha de acesso ao ambiente da ClearSale.</p>
                </div>

               <div class="form-group label-floating is-empty">
                  <input type="text" class="form-control" name="token_prod" required>
                  <label class="control-label" for="inputDefault">Token Produ��o</label> 
                  <p class="help-block">Token de produ��o do ClearSale Start.</p>
                </div>

               <div class="form-group label-floating is-empty">
                  <input type="text" class="form-control" name="token_dev" required>
                  <label class="control-label" for="inputDefault">Token Homologa��o</label> 
                  <p class="help-block">Token de homologa��o do ClearSale Start.</p>
                </div>
               
                <div class="form-group">
                    <div style="text-align: right; padding-top: 10px;">
                      <button type="submit" class="btn btn-primary btn-raised send"><?php echo $staticLabels['send_btn'];?></button>
                      <button type="reset" class="btn btn-default"><?php echo $staticLabels['cancel_btn'];?></button>
                    </div>
                </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</body>
<?php include '../footer.php';?>
</html>
